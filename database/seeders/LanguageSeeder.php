<?php

namespace Database\Seeders;

use App\Models\Language;
use Illuminate\Database\Seeder;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Language::truncate();
        Language::create([
            'name' => 'English',
            'code' => 'en',
            'status' => 1,
        ]);
        Language::create([
            'name' => 'Hindu',
            'code' => 'hi',
            'status' => 1,
        ]);
        Language::create([
            'name' => 'French',
            'code' => 'fr',
            'status' => 0,
        ]);
    }
}
