<?php

namespace Database\Seeders;

use Illuminate\Support\Str;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'John',
            'surname' => 'Cooper',
            'email' => 'johncooper@gmail.com',
            'password' => Hash::make('heslo'),
            'api_key' => Str::random(60),
        ]);
        $user->language()->attach(1);
    }
}
