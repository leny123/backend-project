<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::truncate();
        Role::create([
            'name' => 'Administrator',
            'code' => 'administrator',
            'status' => 1,
        ]);
        Role::create([
            'name' => 'Editor',
            'code' => 'editor',
            'status' => 1,
        ]);
        Role::create([
            'name' => 'Registered user',
            'code' => 'registered',
            'status' => 0,
        ]);
    }
}
