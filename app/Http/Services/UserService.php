<?php

namespace App\Http\Services;

use App\Models\Language;
use App\Models\User;
use Illuminate\Http\JsonResponse;

class UserService {
    public $errors = [];
    public function checkErrors($request) {
        if(!$request->name) {
            array_push($this->errors, 'Name is empty');
        }
        if(!$request->surname) {
            array_push($this->errors, 'Surname is empty');
        }
        if(!$request->password) {
            array_push($this->errors, 'Password is empty');
        }
        if(!$request->email) {
            array_push($this->errors, 'Email is empty');
        }
        if($this->checkUniqueEmail($request->email, $request->header('token'), $request->method())) {
            array_push($this->errors, 'Email is already in use');
        }
        if($request->language) {
            $language = Language::find($request->language);
            if(!$language) {
                array_push($this->errors, 'Language not found.');
            }
            if($language->status == 0) {
                array_push($this->errors, 'This role is inactive.');
            }
        }
        return $this->errors;
    }
    public function checkUniqueEmail($email, $token, $method) {
        $user = User::where('email', $email)->get();
        $currentUser = User::where('api_key', $token)->get();
        if(count($user) == 0) {
            return false;
        }
        if($user[0]->email == $currentUser[0]->email) {
            if($method == 'POST') {
                return true;
            }
            return false;
        }
        return true;
    }
}