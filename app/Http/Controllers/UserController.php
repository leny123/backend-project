<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use App\Http\Requests\UserRequest;
use App\Http\Resources\UsersResource;
use App\Http\Services\UserService;
use App\Models\Language;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //api token check
        $user = User::where('api_key', $request->header('token'))->get();
        if(count($user) == 0) {
            return new JsonResponse(['message' => 'Not auth'], 401);
        }

        return UsersResource::collection(User::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //api token check
        $user = User::where('api_key', $request->header('token'))->get();
        if(count($user) == 0) {
            return new JsonResponse(['message' => 'Not auth'], 401);
        }
        $errors = (new UserService())->checkErrors($request);
        if($errors) {
            return new JsonResponse(['errors' => $errors]);
        }
        
        $newUser = User::create([
            'name' => $request->name,
            'surname' => $request->surname,
            'password' => Hash::make($request->password),
            'email' => $request->email,
            'api_key' => Str::random(60)
        ]);
        $newUser->language()->attach(1);

        return new UsersResource($newUser, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        //api token check
        $user = User::where('api_key', $request->header('token'))->get();
        if(count($user) == 0) {
            return new JsonResponse(['message' => 'Not auth'], 401);
        }

        $user = User::find($id);
        
        if($user) {
            return new UsersResource($user);;
        } else {
            return new JsonResponse(['message' => 'User not found!'], 404);
        }
    }

    public function update(Request $request, $id)
    {
        //api token check
        $user = User::where('api_key', $request->header('token'))->get();
        if(count($user) == 0) {
            return new JsonResponse(['message' => 'Not auth'], 401);
        }

        $user = User::find($id);
        if(!$user) {
            return new JsonResponse(['message' => 'User not found'], 404);
        }
        $errors = (new UserService())->checkErrors($request);
        if($errors) {
            return new JsonResponse(['errors' => $errors]);
        }
        
        if($request->roles) {
            $intIds = array_map('intval', $request->roles);
            $roles = Role::findMany($intIds);

            foreach($roles as $role) {
                if(!$role->status) {
                    return new JsonResponse(['message' => 'This role is inactive.'], 401);
                }
            }
        }
        
        $user->name = $request->name;
        $user->surname = $request->surname;
        $user->password = Hash::make($request->password);
        $user->email = $request->email;
        $user->save(); 

        if($request->roles) {
            $user->roles()->detach();
            foreach($roles as $role) {
                $user->roles()->attach($role);
            }
        }

        if($request->language) {
            $user->language()->detach();
            $user->language()->attach($request->language);
        }
        
        return new JsonResponse(new UsersResource($user));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //api token check
        $user = User::where('api_key', $request->header('token'))->get();
        if(count($user) == 0) {
            return new JsonResponse(['message' => 'Not auth'], 401);
        }

        $user = User::find($id);
        if(!$user) {
            return new JsonResponse(['message' => 'User not found']);
        }
        $user->roles()->detach();
        $user->language()->detach();
        $user->delete();

        return new JsonResponse(['message' => 'User deleted']);
    }
}
